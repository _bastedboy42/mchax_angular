export interface PayerDto {
  ID: number;
  NAME: string;
  PHONE: string;
  ACCOUNT: string;
  ADDRESS1: string;
  ADDRESS2: string;
  ADDRESS3: string;
  ADDRESS4: string;
  BANKID: number;

}
