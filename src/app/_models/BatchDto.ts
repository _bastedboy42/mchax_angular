export interface BatchDto {
  ID: number;
  PAYEENAME: string;
  PAYEEADDRESS1: string;
  PAYEEADDRESS2: string;
  PAYEEADDRESS3: string;
  PAYEEADDRESS4: string;
  PAYEEPHONE: string;
  PAYEEMICR: string;
  PAYERNAME: string;
  PAYERPHONE: string;
  PAYERADDRESS1: string;
  PAYERADDRESS2: string;
  PAYERADDRESS3: string;
  PAYERADDRESS4: string;
  TRANSIT: string;
  CHECKDATE: string;
  AMOUNT: number;
  BANKNAME: number;
  BANKADDRESS1: number;
  BANKADDRESS2: number;
  MEMO: number;
  CHECKNUMBER: number;
  USERID: number;
  SERIALNO: number;
  ACCOUNT: number;
  ROUTING: number;
}
