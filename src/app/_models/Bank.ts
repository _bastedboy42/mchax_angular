export interface Bank {
  ID: number;
  BANKNAME: string;
  BRANCH: string;
  ADDRESS: string;
  CITYSTATEZIP: string;
  ROUTING: string;
  TRANSITSYMBOL: string;
  FILEDATE: Date;
  TYPE: string;
  BRANCHCODE: string;
}


