export interface SettingDto {
  ID: number;
  SURNAME: string;
  SETTINGNAME: string;
  SETTINGVALUE: string;
}

