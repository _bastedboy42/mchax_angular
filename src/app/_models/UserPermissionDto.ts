export interface UserPermissionDto {
    ID: number;
    ISOKMEMORIZE: boolean;
    SIGNATUREMESSAGE: string;
    ISCHAXPERMISSION: boolean;
    USERID: string;
    CANPAYEEENTER: boolean;
    CANPAYEESELECT: boolean;
    CANTRANSACTIONREPORT: boolean;
    CANBATCHPRINT: boolean;
    CANTRANSACTIONDELETE: boolean;
    ISADMIN: boolean;
}
