export interface PayeeDto {
  ID: number;
  NAME: string;
  ADDRESS1: string;
  ADDRESS2: string;
  ADDRESS3: string;
  ADDRESS4: string;
  PHONE: string;
  ISDEFAULT: boolean;
}


