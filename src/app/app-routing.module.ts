import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DefaultComponent} from './layouts/default/default.component';
import {DashboardComponent} from './modules/dashboard/dashboard.component';
import {PostsComponent} from './modules/posts/posts.component';
import {SignupComponent} from './signup/signup.component';
import {ForgotpasswordComponent} from './forgotpassword/forgotpassword.component';
import {LoginComponent} from './login/login.component';
import {BankAccountsComponent} from './modules/bank-accounts/bank-accounts.component';
import {UsersComponent} from './modules/users/users.component';
import {ChecksComponent} from './modules/checks/checks.component';
import {ReportsComponent} from './modules/reports/reports.component';
import {PrintPreviewComponent} from './print-preview/print-preview.component';
import {AuthGuardService} from './_guards/auth-guard.service';
import {PayeesComponent} from './modules/payees/payees.component';
import {PayersComponent} from './modules/payers/payers.component';
import {SettingsComponent} from './modules/settings/settings.component';
import {TransactionsComponent} from './modules/transactions/transactions.component';


const routes: Routes = [
    {
      path: '', component: DefaultComponent,
      children:
        [
          {
            path: '',
            component: DashboardComponent
          },
          {path: 'bank-accounts', component: BankAccountsComponent},
          {path: 'users', component: UsersComponent},
          {path: 'payees', component: PayeesComponent},
          {path: 'payers', component: PayersComponent},
          {path: 'checks', component: ChecksComponent},
          {path: 'reports', component: ReportsComponent},
          {path: 'settings', component: SettingsComponent},
          {path: 'transactions', component: TransactionsComponent}
        ]
      , canActivate: [AuthGuardService]

    },
    {path: 'dashboard', component: DashboardComponent},
    {path: 'login', component: LoginComponent},
    {path: 'signup', component: SignupComponent},
    {path: 'forgotpassword', component: ForgotpasswordComponent},
    {path: 'users', component: UsersComponent},
    {path: 'posts', component: PostsComponent},
    {path: 'print-preview', component: PrintPreviewComponent},


  ]
;

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
