import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import {first} from 'rxjs/operators';
import {AuthenticationService} from '../_services/authentication.service';


import notify from 'devextreme/ui/notify';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor(private  router: Router, private  auth: AuthenticationService) {
  }

  ngOnInit(): void {

  }

  OnCancel() {
    this.router.navigate(['/login']);
  }


  onSubmit(f: NgForm, $event: Event) {
    this.auth.register(f.value).pipe(first()).subscribe(response => {
        notify('Registration Successful', 'success', 3000);
        this.OnCancel();
      },
      (error) => {
        notify('Registration Failed', 'error', 3000);
      });
    f.reset();
  }
}
