import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, NgForm} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthenticationService} from '../_services/authentication.service';
import {AlertService} from '../_services/alert.service';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @ViewChild('f', {static: false}) loginForm: NgForm;

  loading = false;
  submitted = false;
  returnUrl: string;
  rememberMe: false;
  errorMessage: string;


  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private authenticationService: AuthenticationService,
              private alertService: AlertService) {

    if (this.authenticationService.currentUserValue) {
      router.navigate(['/']);
    }
  }

  // get f() {
  //   return this.loginForm.controls;
  // }

  ngOnInit(): void {
    // this.loginForm = this.formBuilder.group({
    //   username: ['', Validators.required],
    //   password: ['', Validators.required],
    // });
    // this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }


  onSubmit() {
    this.submitted = true;
    this.alertService.clear();
    if (this.loginForm.invalid) {
      return;
    }
    this.authenticationService.login(this.loginForm.value.userData.email, this.loginForm.value.userData.password).pipe(first()).subscribe(data => {
        this.router.navigate(['/']);
      },
      error => {
        this.errorMessage = error;
        this.alertService.error(error);
      });
    this.submitted = false;
  }

  forgotpassword() {
    this.router.navigate(['/forgotpassword']);
  }


  loggedIn() {
    const token = localStorage.getItem('token');
    return !!token;
  }

  helloWorld() {
    alert('hello');
  }

  testClick() {
    alert(this.rememberMe);
  }
}
