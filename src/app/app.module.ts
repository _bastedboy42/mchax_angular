// Core
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {MatButtonModule} from '@angular/material/button';
import {RouterModule} from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatTableModule} from '@angular/material/table';
import {MatIconModule} from '@angular/material/icon';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {JwtModule} from '@auth0/angular-jwt';
// PrimePng
import {InputTextModule} from 'primeng/inputtext';
import {ButtonModule} from 'primeng/button';
import {TableModule} from 'primeng/table';
import {DialogModule} from 'primeng/dialog';
import {CheckboxModule} from 'primeng';
// DevExpress
import {DxReportViewerModule} from 'devexpress-reporting-angular';
import {DxDataGridModule, DxBulletModule, DxTemplateModule, DxTextBoxModule} from 'devextreme-angular';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {AlertComponent} from './_components/alert.component';
import {JwtInterceptor} from './_helpers/jwt.interceptor';
import {ErrorInterceptor} from './_helpers/error.interceptor';
import {fakeBackendProvider} from './_helpers/fake-backend';
import {HomeComponent} from './home/home.component';
import {ForgotpasswordComponent} from './forgotpassword/forgotpassword.component';
import {SignupComponent} from './signup/signup.component';
import {DefaultModule} from './layouts/default/default.module';
import {SharedModule} from './shared/shared.module';
import {BankAccountsComponent} from './modules/bank-accounts/bank-accounts.component';
import {UsersComponent} from './modules/users/users.component';
import {ChecksComponent} from './modules/checks/checks.component';
import {ReportsComponent} from './modules/reports/reports.component';
import {PrintPreviewComponent} from './print-preview/print-preview.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import {DxButtonModule} from 'devextreme-angular';
import { DatagridComponent } from './datagrid/datagrid.component';
import { PayeesComponent } from './modules/payees/payees.component';
import { PayersComponent } from './modules/payers/payers.component';
import { SettingsComponent } from './modules/settings/settings.component';
import { TransactionsComponent } from './modules/transactions/transactions.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AlertComponent,
    HomeComponent,
    ForgotpasswordComponent,
    SignupComponent,
    BankAccountsComponent,
    UsersComponent,
    ChecksComponent,
    ReportsComponent,
    PrintPreviewComponent,
    DatagridComponent,
    PayeesComponent,
    PayersComponent,
    SettingsComponent,
    TransactionsComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    TableModule,
    HttpClientModule,
    InputTextModule,
    DialogModule,
    ButtonModule,
    ReactiveFormsModule,
    RouterModule,
    AppRoutingModule,
    CheckboxModule,
    MatButtonModule,
    DefaultModule,
    SharedModule,
    MatTableModule,
    MatIconModule,
    MatFormFieldModule,
    MatDatepickerModule,
    DxReportViewerModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: () => {
          return localStorage.getItem('token');
        },
        allowedDomains: ['http://localhost:5000'],
        disallowedRoutes: ['http://localhost:5000/api/auth'],
      },
    }),
    MatPaginatorModule,
    DxButtonModule,
    DxDataGridModule,
    DxTemplateModule,
    DxBulletModule,
    DxTextBoxModule
  ],

  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
    fakeBackendProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule
{
}
