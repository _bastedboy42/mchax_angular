import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {AlertService} from '../_services/alert.service';


@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit, OnDestroy {
  private subscription: Subscription;
  message: any;

  constructor(private alertService: AlertService) {
  }

  ngOnInit(): void {
    this.subscription = this.alertService.getAlert().subscribe(setMessageCss
    );

    function setMessageCss(message) {
      switch (message && message.type) {
        case 'success':
          this.message.cssClass = 'alert alert-success';
          break;
        case 'error':
          this.message.cssClass = 'alert alert-danger';
          break;
      }
      this.message = message;
    }
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
