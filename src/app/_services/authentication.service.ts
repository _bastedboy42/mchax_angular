import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {User} from '../_models/User';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {JwtHelperService} from '@auth0/angular-jwt';
import {UserDto} from '../_models/UserDto';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  public currentUser: Observable<User>;
  jwtHelper = new JwtHelperService();
  decodedToken: any;
  private currentUserSubject: BehaviorSubject<User>;

  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  login(email, password) {
    return this.http.post<any>(`${environment.apiUrl}auth/authenticate`, {email, password})
      .pipe(map(user => {
        localStorage.setItem('token', JSON.stringify(user));
        this.decodedToken = this.jwtHelper.decodeToken(user.token);
        console.log(this.decodedToken);
        this.currentUserSubject.next(user);
        return user;
      }));
  }


  loggedIn() {
    const token = localStorage.getItem('token');
    return !this.jwtHelper.isTokenExpired(token);
  }


  logout() {
    localStorage.removeItem('token');
    this.currentUserSubject.next(null);
  }

  register(model: UserDto) {
    return this.http.post<any>(`${environment.apiUrl}auth/register`, model);
  }
}
