import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {SettingDto} from '../_models/SettingDto';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SettingService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) {
  }


  getAllSettings(): Observable<SettingDto[]> {
    return this.http.get<SettingDto[]>(this.baseUrl + 'setting');
  }

  getSettingById(id: number) {
    return this.http.get<SettingDto>(this.baseUrl + `setting/${id}`);
  }

  deleteSetting(id: number) {
    return this.http.delete(this.baseUrl + `setting/${id}`);
  }

  createSetting(setting: SettingDto) {
    return this.http.post(this.baseUrl + 'create', setting);
  }

  updateSetting(id: number, setting: SettingDto) {
    return this.http.put(this.baseUrl + `setting/${id}`, setting);
  }


}
