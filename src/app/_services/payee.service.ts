import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {PayeeDto} from '../_models/PayeeDto';

@Injectable({
  providedIn: 'root'
})
export class PayeeService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) {
  }

  getAllPayees(): Observable<PayeeDto[]> {
    return this.http.get<PayeeDto[]>(this.baseUrl + 'payee');
  }


  getPayeeById(id: number) {
    return this.http.get<PayeeDto>(this.baseUrl + `payee/${id}`);
  }

  deletePayee(id: number) {
    return this.http.delete(this.baseUrl + `payee/${id}`);
  }

  createPayee(payee: PayeeDto) {
    return this.http.post(this.baseUrl + 'create', payee);
  }

  updatePayee(id: number, payee: PayeeDto) {
    return this.http.put(this.baseUrl + `payee/${id}`, payee);
  }
}
