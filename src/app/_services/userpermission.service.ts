import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {UserPermissionDto} from '../_models/UserPermissionDto';

@Injectable({
  providedIn: 'root'
})
export class UserpermissionService {

  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) {
  }

  getUserPermissions(): Observable<UserPermissionDto[]> {
    return this.http.get<UserPermissionDto[]>(this.baseUrl + 'userPermission/');
  }

  getUsergetUserPermissionsById(id: number) {
    return this.http.get<UserPermissionDto>(this.baseUrl + `userPermission/${id}`);
  }

  deleteUserPermissions(id: number) {
    return this.http.delete(this.baseUrl + `userPermission/${id}`);
  }

  createUserPermissions(user: UserPermissionDto) {
    return this.http.post(this.baseUrl + 'userPermission/create', user);
  }


  updateUserPermissions(id: number, user: UserPermissionDto) {
    return this.http.post(this.baseUrl + `userPermission/${id}`, user);
  }
}
