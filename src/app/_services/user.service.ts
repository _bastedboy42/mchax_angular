import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {UserDto} from '../_models/UserDto';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) {
  }

  getUsers(): Observable<UserDto[]> {
    return this.http.get<UserDto[]>(this.baseUrl + 'user/');
  }

  getUserById(id: number) {
    return this.http.get<UserDto>(this.baseUrl + `user/${id}`);
  }

  deleteUser(id: number) {
    return this.http.delete(this.baseUrl + `user/${id}`);
  }

  create(user: UserDto) {
    return this.http.post(this.baseUrl + 'user/create', user);
  }

  updateUser(id: number, user: UserDto) {
    return this.http.post(this.baseUrl + `user/${id}`, user);
  }
}
