import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {BatchDto} from '../_models/BatchDto';

@Injectable({
  providedIn: 'root'
})
export class BatchService {
  baseUrl = environment.apiUrl;

  constructor(private  http: HttpClient) {
  }

  getBatchChecks(): Observable<BatchDto[]> {
    return this.http.get<BatchDto[]>(this.baseUrl + 'batch');
  }

  getBatchById(id: number) {
    return this.http.get<BatchDto>(this.baseUrl + `batch/${id}`);
  }

  deleteBatch(id: number) {
    return this.http.delete(this.baseUrl + `batch/${id}`);
  }

  createBatch(batch: BatchDto) {
    return this.http.post(this.baseUrl + 'create', batch);
  }

  updateBatch(id: number, batch: BatchDto) {
    return this.http.put(this.baseUrl + `batch/${id}`, batch);
  }
}
