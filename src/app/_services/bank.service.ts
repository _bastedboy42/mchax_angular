import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {Bank} from '../_models/Bank';

@Injectable({
  providedIn: 'root'
})
export class BankService {
  baseUrl = environment.apiUrl;

  constructor(private  http: HttpClient) {
  }

  getBanks(): Observable<Bank[]> {
    return this.http.get<Bank[]>(this.baseUrl + 'bank/');
  }


  getBank(id: number): Observable<Bank> {
    return this.http.get<Bank>(this.baseUrl + `bank/${id}`);
  }

  delete(id: number) {
    return this.http.delete(this.baseUrl + `bank/${id}`);
  }

  create(bank: Bank) {
    return this.http.post(this.baseUrl + 'create', bank);
  }

  update(id: number, bank: Bank) {
    return this.http.put(this.baseUrl + `bank/${id}`, bank);
  }
}
