import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {TransactionDto} from '../_models/TransactionDto';

@Injectable({
  providedIn: 'root'
})
export class TransactionService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) {
  }

  getAllTransactions(): Observable<TransactionDto[]> {
    return this.http.get<TransactionDto[]>(this.baseUrl + 'transaction');
  }

  getTransactionByDate(date: Date) {
    return this.http.get<TransactionDto[]>(this.baseUrl + 'transaction/date');
  }

  getTransactinById(id: number) {
    return this.http.get<TransactionDto>(this.baseUrl + `transaction/${id}`);
  }

  deleteTransaction(id: number) {
    return this.http.delete(this.baseUrl + `${id}`);
  }

  createTransaction(transaction: TransactionDto) {
    return this.http.post(this.baseUrl + 'create', transaction);
  }
}
