import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {PayerDto} from '../_models/PayerDto';

@Injectable({
  providedIn: 'root'
})
export class PayerService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) {
  }


  getAllPayers(): Observable<PayerDto[]> {
    return this.http.get<PayerDto[]>(this.baseUrl + 'payer');
  }

  getPayerById(id: number) {
    return this.http.get<PayerDto>(this.baseUrl + `payer/${id}`);
  }

  deletePayer(id: number) {
    return this.http.delete(this.baseUrl + `payer/${id}`);
  }

  createPayer(payer: PayerDto) {
    return this.http.post(this.baseUrl + 'create', payer);
  }

  updatePayer(id: number, payer: PayerDto) {
    return this.http.put(this.baseUrl + `payer/${id}`, payer);
  }
}
