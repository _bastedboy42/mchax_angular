import {Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-print-preview',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './print-preview.component.html',
  styleUrls: [
     '../../../node_modules/jquery-ui/themes/base/all.css',
     '../../../node_modules/@devexpress/analytics-core/dist/css/dx-analytics.common.css',
     '../../../node_modules/@devexpress/analytics-core/dist/css/dx-analytics.light.css',
     '../../../node_modules/devexpress-reporting/dist/css/dx-webdocumentviewer.css'
  ]
})
export class PrintPreviewComponent implements OnInit {

  reportUrl = 'CheckLayoutBatchPrinting';
  hostUrl = 'http://localhost:5000/';
  invokeAction = 'DXXRDV';

  constructor() { }

  ngOnInit(): void {
  }
}
