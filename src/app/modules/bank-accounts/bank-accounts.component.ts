import {Component, OnInit} from '@angular/core';
import {BankService} from '../../_services/bank.service';
import {Bank} from '../../_models/Bank';

@Component({
  selector: 'app-bank-accounts',
  templateUrl: './bank-accounts.component.html',
  styleUrls: ['./bank-accounts.component.css']
})
export class BankAccountsComponent implements OnInit {
  public dataSource: Bank[];
  collapsed = false;

  constructor(private bankService: BankService) {

  }

  contentReady = (e) => {
    if (!this.collapsed) {
      this.collapsed = true;
      e.component.expandRow(['EnviroCare']);
    }
  };
  customizeTooltip = (pointsInfo) => {
    return {text: parseInt(pointsInfo.originalValue) + '%'};
  };


  ngOnInit(): void {
    this.bankService.getBanks().subscribe((response) => {
      this.dataSource = response;
      console.log(response);
    });
  }

  createBank(bank: Bank) {
   return this.bankService.create(bank).subscribe(response => {
      console.log('Bank created');
    });
  }


  deleteBank(id: number) {
    this.bankService.delete(id).subscribe(response => {
      console.log('A record was deleted');
    });
  }
}
