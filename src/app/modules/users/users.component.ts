import {Component, OnInit} from '@angular/core';
import {UserService} from '../../_services/user.service';
import {UserDto} from '../../_models/UserDto';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  dataSource: UserDto[];
  collapsed = false;
  constructor(private userService: UserService) {
  }

  ngOnInit(): void {
    this.userService.getUsers().subscribe((response) => {
      this.dataSource = response;
    });
  }

  contentReady = (e) => {
    if (!this.collapsed) {
      this.collapsed = true;
      e.component.expandRow(['EnviroCare']);
    }
  };
  customizeTooltip = (pointsInfo) => {
    return {text: parseInt(pointsInfo.originalValue) + '%'};
  };

}
