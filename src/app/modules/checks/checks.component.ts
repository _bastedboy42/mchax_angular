import {Component, OnInit} from '@angular/core';
import {TransactionDto} from '../../_models/TransactionDto';
import {TransactionService} from '../../_services/transaction.service';

@Component({
  selector: 'app-checks',
  templateUrl: './checks.component.html',
  styleUrls: ['./checks.component.css']
})
export class ChecksComponent implements OnInit {

  dataSource: TransactionDto[];
  collapsed = false;

  constructor(private transactionService: TransactionService) {
  }

  ngOnInit(): void {
    // this.transactionService.getTransaction;
  }

  contentReady = (e) => {
    if (!this.collapsed) {
      this.collapsed = true;
      e.component.expandRow(['EnviroCare']);
    }
  };

  customizeTooltip = (pointsInfo) => {
    return {text: parseInt(pointsInfo.originalValue) + '%'};
  };

}
